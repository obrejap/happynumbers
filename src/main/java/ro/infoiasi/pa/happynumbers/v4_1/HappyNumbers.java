package ro.infoiasi.pa.happynumbers.v4_1;

import java.util.Set;
import java.util.TreeSet;


public class HappyNumbers {
	private static int MAX_HAPPY_NUMBER = 5000;
	
	private int upperBound;
	private Set<Integer> happyNumbers;
	
	public HappyNumbers(int upperBound) {
		validateUpperBound(upperBound);
		this.upperBound = upperBound;
		this.happyNumbers = new TreeSet<Integer>();
		computeHappyNumbers();
	}
	
	private void validateUpperBound(int upperBound){
		if (upperBound <= 0) {
			throw new IllegalStateException(
					"The upper bound must be greater than zero.");
		}
		if (upperBound > MAX_HAPPY_NUMBER) {
			throw new IllegalStateException(
					String.format("This class can compute happy numbers up to %d",
					MAX_HAPPY_NUMBER));
		}
	}

	private void computeHappyNumbers(){		
		for (int i = 1; i < upperBound; i++) {
			Set<Integer> sequenceOfNumbers = new TreeSet<Integer>();
			int currentNumber = i;
			do {
				sequenceOfNumbers.add(currentNumber);
				int numberOfDigits = (int) (Math.log10(currentNumber) + 1);
				int[] digitsOfCurrentNumber = new int[numberOfDigits];
				for (int j = 0, number = currentNumber; j < numberOfDigits; j++, number = number / 10) {
					digitsOfCurrentNumber[j] = number % 10;
				}
				int sum = 0;
				for (int j = 0; j < numberOfDigits; j++) {
					sum += digitsOfCurrentNumber[j] * digitsOfCurrentNumber[j];
				}
				if (sum == 1) { // currentNumber and all numbers in sequence are happy numbers!
					happyNumbers.addAll(sequenceOfNumbers);
					break; // do-while
				} else {
					// check if current 'sum' is not in the sequence
					if (sequenceOfNumbers.contains(sum)) { // we have a cycle
						break;
					}
					// if not a sequence, iterate and check if the new 'sum' is not a happy number
					currentNumber = sum;
				}
			} while (true);
		}		
	}

	public int getUpperBound() {
		return upperBound;
	}

	public Set<Integer> getHappyNumbers() {
		return happyNumbers;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("The happy numbers from 1 to ").append(upperBound).append(" are:\n");
		stringBuilder.append(happyNumbers);
		return stringBuilder.toString();
	}
}
