package ro.infoiasi.pa.happynumbers.v5_1;

public class IntegerWrapper implements Comparable<IntegerWrapper>{

	private final Integer value;
	private int[] digits;

	public IntegerWrapper(Integer value) {
		this.value = value;
		computeDigits();
	}
	
	private void computeDigits(){
		int numberOfDigits = (int) (Math.log10(value) + 1);
		digits = new int[numberOfDigits];
		for (int j = 0, number = value; j < numberOfDigits; j++, number = number / 10) {
			digits[j] = number % 10;
		}
	}

	public Integer getValue() {
		return value;
	}
	
	public IntegerWrapper getSumOfSquaredDigits(){
		int sum = 0;
		for (int j = 0; j < digits.length; j++) {
			sum += digits[j] * digits[j];
		}
		return new IntegerWrapper(sum);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntegerWrapper other = (IntegerWrapper) obj;
		if (value != other.value)
			return false;
		return true;
	}

	public int compareTo(IntegerWrapper obj) {
		if(this==obj){
			return 0;
		}
		if(obj==null){
			return 1;
		}
		return this.value-obj.value;
	}

	@Override
	public String toString() {
		return value.toString();
	}
}
