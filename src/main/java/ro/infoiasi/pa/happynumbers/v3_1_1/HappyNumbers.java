package ro.infoiasi.pa.happynumbers.v3_1_1;


public class HappyNumbers {
	private static int MAX_HAPPY_NUMBER = 5000;
	
	private int upperBound;
	private int[] happyNumbers;
	
	public HappyNumbers(int upperBound) {
		validateUpperBound(upperBound);
		this.upperBound = upperBound;
		computeHappyNumbers();
	}
	
	private void validateUpperBound(int upperBound){
		if (upperBound <= 0) {
			throw new IllegalStateException(
					"The upper bound must be greater than zero.");
		}
		if (upperBound > MAX_HAPPY_NUMBER) {
			throw new IllegalStateException(
					String.format("This class can compute happy numbers up to %d",
					MAX_HAPPY_NUMBER));
		}
	}

	private void computeHappyNumbers(){
		this.happyNumbers = new int[MAX_HAPPY_NUMBER/5]; // I hope 1/5 of max happy numbers is enough
		int indexOfHappyNumbers = 0;
		OUTHERMOST_FOR: for (int i = 1; i < upperBound; i++) {
			int sequenceOfNumbers[] = new int[100]; // More hope
			int indexOfSequence = 0;
			int currentNumber = i;
			do {
				sequenceOfNumbers[indexOfSequence] = currentNumber;
				int numberOfDigits = (int) (Math.log10(currentNumber) + 1);
				int[] digitsOfCurrentNumber = new int[numberOfDigits];
				for (int j = 0, number = currentNumber; j < numberOfDigits; j++, number = number / 10) {
					digitsOfCurrentNumber[j] = number % 10;
				}
				int sum = 0;
				for (int j = 0; j < numberOfDigits; j++) {
					sum += digitsOfCurrentNumber[j] * digitsOfCurrentNumber[j];
				}
				if (sum == 1) { // currentNumber is a happyNumber!
					happyNumbers[indexOfHappyNumbers++] = i;
					break; // do-while
				} else {
					// check if current 'sum' is not in the sequence
					for (int k = 0; k < indexOfSequence; k++) {
						if (sequenceOfNumbers[k] == sum) { // we have a cycle
							continue OUTHERMOST_FOR;
						}
					}
					// if not a sequence, iterate and check if the new 'sum' is not a happy number
					indexOfSequence++;					
					currentNumber = sum;
				}
			} while (true);
		}		
	}

	public int getUpperBound() {
		return upperBound;
	}

	public int[] getHappyNumbers() {
		return happyNumbers;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("The happy numbers from 1 to ").append(upperBound).append(" are:\n");
		for (int i = 0; i < happyNumbers.length; i++) {
			if(happyNumbers[i]==0){
				break;
			}
			stringBuilder.append(happyNumbers[i]).append(" ");
		}
		return stringBuilder.toString();
	}
}
