package ro.infoiasi.pa.happynumbers.v5;

public class IntegerWrapper implements Comparable<IntegerWrapper>{

	private final Integer value;

	public IntegerWrapper(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntegerWrapper other = (IntegerWrapper) obj;
		if (value != other.value)
			return false;
		return true;
	}

	public int compareTo(IntegerWrapper obj) {
		if(this==obj){
			return 0;
		}
		if(obj==null){
			return 1;
		}
		return this.value-obj.value;
	}

	@Override
	public String toString() {
		return value.toString();
	}
}
