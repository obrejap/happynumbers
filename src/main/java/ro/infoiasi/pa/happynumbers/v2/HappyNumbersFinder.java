package ro.infoiasi.pa.happynumbers.v2;


public class HappyNumbersFinder {

	public static void main(String[] args) {
		int upperBound = getAndValidateArguments(args);
		int[] happyNumbers = computeHappyNumbers(upperBound);
		printHappyNumbers(upperBound, happyNumbers);
	}
	
	private static int getAndValidateArguments(String[] args){
		if (args.length != 1) {
			throw new IllegalStateException("The 'Happy numbers' program needs an argument: the upper bound for the happy numbers.");			
		}
		int upperBound = 0;
		try {
			upperBound = Integer.valueOf(args[0]).intValue();
		} catch (NumberFormatException e) {
			//throw new IllegatStateException and append the original NumberFormatException to it
			throw new IllegalStateException(String.format("The argument '%s' is not a number", args[0]), e);
		}
		if (upperBound <= 0) {
			throw new IllegalStateException("The upper bound must be greater than zero.");
		}
		return upperBound;
	}
	
	private static int[] computeHappyNumbers(int upperBound){
		int[] happyNumbers = new int[150]; // I hope 150 is enough
		int indexOfHappyNumbers = 0;
		OUTHERMOST_FOR: for (int i = 1; i < upperBound; i++) {
			int sequenceOfNumbers[] = new int[100]; // More hope
			int indexOfSequence = 0;
			int currentNumber = i;
			do {
				sequenceOfNumbers[indexOfSequence] = currentNumber;
				int numberOfDigits = (int) (Math.log10(currentNumber) + 1);
				int[] digitsOfCurrentNumber = new int[numberOfDigits];
				for (int j = 0, number = currentNumber; j < numberOfDigits; j++, number = number / 10) {
					digitsOfCurrentNumber[j] = number % 10;
				}
				int sum = 0;
				for (int j = 0; j < numberOfDigits; j++) {
					sum += digitsOfCurrentNumber[j] * digitsOfCurrentNumber[j];
				}
				if (sum == 1) { // currentNumber is a happyNumber!
					happyNumbers[indexOfHappyNumbers++] = i;
					break; // do-while
				} else {
					// check if current 'sum' is not in the sequence
					for (int k = 0; k < indexOfSequence; k++) {
						if (sequenceOfNumbers[k] == sum) { // we have a cycle
							continue OUTHERMOST_FOR;
						}
					}
					// if not a sequence, iterate and check if the new 'sum' is not a happy number
					indexOfSequence++;					
					currentNumber = sum;
				}
			} while (true);
		}		
		return happyNumbers;
	}
	
	private static void printHappyNumbers(int upperBound, int[] happyNumbers){
		System.out.printf("The happy numbers from 1 to %d are:\n", upperBound);
		for (int i = 0; i < happyNumbers.length; i++) {
			if(happyNumbers[i]==0){
				break;
			}
			System.out.printf("%d ", happyNumbers[i]);
		}
	}

}
