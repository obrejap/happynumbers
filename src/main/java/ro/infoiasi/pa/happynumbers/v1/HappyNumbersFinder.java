package ro.infoiasi.pa.happynumbers.v1;


public class HappyNumbersFinder {

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("The 'Happy numbers' program needs an argument: the upper bound for the happy numbers.");
			return;
		}
		int upperBound = 0;
		try {
			upperBound = Integer.valueOf(args[0]).intValue();
		} catch (NumberFormatException e) {
			System.err.printf("The argument '%s' is not a number.\n", args[0]);
			return;
		}
		if (upperBound <= 0) {
			System.err.println("The upper bound must be greater than zero.");
			return;
		}
		int[] happyNumbers = new int[150]; // I hope 150 is enough
		int indexOfHappyNumbers = 0;
		OUTHERMOST_FOR: for (int i = 1; i < upperBound; i++) {
			int sequenceOfNumbers[] = new int[100]; // More hope
			int indexOfSequence = 0;
			int currentNumber = i;
			do {
				sequenceOfNumbers[indexOfSequence] = currentNumber;
				int numberOfDigits = (int) (Math.log10(currentNumber) + 1);
				int[] digitsOfCurrentNumber = new int[numberOfDigits];
				for (int j = 0, number = currentNumber; j < numberOfDigits; j++, number = number / 10) {
					digitsOfCurrentNumber[j] = number % 10;
				}
				int sum = 0;
				for (int j = 0; j < numberOfDigits; j++) {
					sum += digitsOfCurrentNumber[j] * digitsOfCurrentNumber[j];
				}
				if (sum == 1) { // currentNumber is a happyNumber!
					happyNumbers[indexOfHappyNumbers++] = i;
					break; // do-while
				} else {
					// check if current 'sum' is not in the sequence
					for (int k = 0; k < indexOfSequence; k++) {
						if (sequenceOfNumbers[k] == sum) { // we have a cycle
							continue OUTHERMOST_FOR;
						}
					}
					// if not a sequence, iterate and check if the new 'sum' is not a happy number
					indexOfSequence++;					
					currentNumber = sum;
				}
			} while (true);
		}
		System.out.printf("The happy numbers from 1 to %d are:\n", upperBound);
		for (int i = 0; i < indexOfHappyNumbers; i++) {
			System.out.printf("%d ", happyNumbers[i]);
		}
	}

}
