package ro.infoiasi.pa.happynumbers.v3;


public class HappyNumbersFinder {

	public static void main(String[] args) {
		int upperBound = getAndValidateArguments(args);
		HappyNumbers happyNumbers = new HappyNumbers(upperBound);
		printHappyNumbers(happyNumbers.getUpperBound(), happyNumbers.getHappyNumbers());
	}
	
	private static int getAndValidateArguments(String[] args){
		if (args.length != 1) {
			throw new IllegalStateException("The 'Happy numbers' program needs an argument: the upper bound for the happy numbers.");			
		}
		int upperBound = 0;
		try {
			upperBound = Integer.valueOf(args[0]).intValue();
		} catch (NumberFormatException e) {
			//throw new IllegatStateException and append the original NumberFormatException to it
			throw new IllegalStateException(String.format("The argument '%s' is not a number", args[0]), e);
		}
		if (upperBound <= 0) {
			throw new IllegalStateException("The upper bound must be greater than zero.");
		}
		return upperBound;
	}
	
	private static void printHappyNumbers(int upperBound, int[] happyNumbers){
		System.out.printf("The happy numbers from 1 to %d are:\n", upperBound);
		for (int i = 0; i < happyNumbers.length; i++) {
			if(happyNumbers[i]==0){
				break;
			}
			System.out.printf("%d ", happyNumbers[i]);
		}
	}

}
