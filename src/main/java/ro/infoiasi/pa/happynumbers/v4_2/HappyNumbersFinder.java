package ro.infoiasi.pa.happynumbers.v4_2;


public class HappyNumbersFinder {

	public static void main(String[] args) {
		int upperBound = getAndValidateArguments(args);
		HappyNumbers happyNumbers = new HappyNumbers(upperBound);
		System.out.println(happyNumbers);
	}
	
	private static int getAndValidateArguments(String[] args){
		if (args.length != 1) {
			throw new IllegalStateException("The 'Happy numbers' program needs an argument: the upper bound for the happy numbers.");			
		}
		int upperBound = 0;
		try {
			upperBound = Integer.valueOf(args[0]).intValue();
		} catch (NumberFormatException e) {
			//throw new IllegatStateException and append the original NumberFormatException to it
			throw new IllegalStateException(String.format("The argument '%s' is not a number", args[0]), e);
		}
		return upperBound;
	}

}
